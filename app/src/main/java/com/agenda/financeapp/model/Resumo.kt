package com.agenda.financeapp.model

import java.math.BigDecimal

class Resumo(private val transacoes: List<Transacao>) {

    val despesa: BigDecimal get() = somaPor(Tipo.DESPESA)
    val receita: BigDecimal get() = somaPor(Tipo.RECEITA)
    val total: BigDecimal   get() = receita.subtract(despesa)

    fun somaPor(tipo: Tipo): BigDecimal {

        val somaDeTransacoesPeloTipo = transacoes
            .filter { it.tipo == tipo }
            .sumByDouble { it.valor.toDouble() }

        return BigDecimal(somaDeTransacoesPeloTipo)
    }
}