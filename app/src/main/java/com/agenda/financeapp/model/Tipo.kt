package com.agenda.financeapp.model

enum class Tipo{
    RECEITA, DESPESA
}