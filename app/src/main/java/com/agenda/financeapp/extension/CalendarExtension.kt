package com.agenda.financeapp.extension

import java.text.SimpleDateFormat
import java.util.*

//colocando a minha função na classe Calendar "cuidado ao adicionar funções a classes que"
fun Calendar.formataParaBrasilerio(): String {
    val formatoBrasileiro = "dd/MM/yyyy"
    val format = SimpleDateFormat(formatoBrasileiro)
    return format.format(this.time)
}