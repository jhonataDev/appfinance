package com.agenda.financeapp.ui

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.View
import com.agenda.financeapp.R
import com.agenda.financeapp.extension.formataParaBrasileiro
import com.agenda.financeapp.model.Resumo
import com.agenda.financeapp.model.Transacao
import kotlinx.android.synthetic.main.resumo_card.view.*
import java.math.BigDecimal

class ResumoView(context: Context,
    private val view: View,
    transacao: List<Transacao>
) {

    private val resumo: Resumo = Resumo(transacao)
    private val corReceita = ContextCompat.getColor(context, R.color.receita)
    private val corDespesa = ContextCompat.getColor(context, R.color.despesa)

    fun atualiza() {
        adicionaReceita()
        adicionaDespesa()
        adicionaTotal()
    }

    private fun adicionaReceita() {
        val totalReceita = resumo.receita

        with(view.resumo_card_receita) {
            setTextColor(corReceita)
            text = totalReceita.formataParaBrasileiro()
        }

    }

    private fun adicionaDespesa() {
        val totalDespesa = resumo.despesa

        with(view.resumo_card_despesa) {
            setTextColor(corDespesa)
            text = totalDespesa.formataParaBrasileiro()
        }

    }

    private fun adicionaTotal() {
        val total = resumo.total
        val cor = corPor(total)


        with(view.resumo_card_total) {
            setTextColor(cor)
            text = total.formataParaBrasileiro()
        }

    }

    private fun corPor(valor: BigDecimal): Int {
        if (valor >= BigDecimal.ZERO) {
            return corReceita
        }
        return corDespesa
    }
}